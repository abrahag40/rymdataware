import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './components/shared/layout/layout.component';


const routes: Routes = [
  { path: '', redirectTo: 'Home', pathMatch: 'full'},
  { path: '', component: LayoutComponent, children: [
      { path: 'Home', component: HomeComponent },
    ]
  }
];

export const routing = RouterModule.forRoot(routes, { useHash: true });