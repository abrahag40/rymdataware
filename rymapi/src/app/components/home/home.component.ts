import { Component, OnInit, ViewChild } from '@angular/core';
import { DataBindingDirective, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { process } from '@progress/kendo-data-query';
import { Personajes } from '../../models/personaje.model';
import { PersonajeService } from '../../services/personajes.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  @ViewChild(DataBindingDirective) dataBinding: DataBindingDirective;
  public gridData: any[];
  public URLImg: string;
  public pageSize = 3;
  public skip = 0;
  public gridView: any[];



  constructor(private _personaje: PersonajeService) { }

  public ngOnInit(): void {
    this.loadPersonajes();
  }

  loadPersonajes() {
    this._personaje.getPersonajes().subscribe(
      res => {
        this.gridData = res.results;
        // console.log("rol ",this.gridDataRoles);
      });
  }

  public photoURL(image: any): string {
    this.URLImg = image;
    return this.URLImg;
  }

  public onFilter(inputValue: string): void {
    this.gridView = process(this.gridData, {
      filter: {
        logic: "or",
        filters: [
          {
            field: 'name',
            operator: 'contains',
            value: inputValue
          },
          {
            field: 'species',
            operator: 'contains',
            value: inputValue
          }
        ],
      }
    }).data;

    this.dataBinding.skip = 0;
  }

}
