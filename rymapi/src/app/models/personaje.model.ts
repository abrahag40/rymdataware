export interface Personajes {
    info: Array<info>,
    results: Array<results>

}

export interface info {
    count: number,
    pages: number,
    next: string, 
    prev: boolean
}

export interface results {
    id: number,
    name: string,
    status: string,
    species: string,
    type: string,
    gender: string,
    origin: Array<origin>,
    location: Array<location>,
    image: string,
    episode: string,
    url: string,
    created: string
}

export interface origin {
    name: number,
    url: string,
}

export interface location {
    name: number,
    url: string,
}